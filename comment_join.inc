<?php


//admin interface
function comment_join_settings_form() {
  $form = array();
  $form['comment_join'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Comment join module settings'),
    '#tree'  => TRUE
  );
  $form['comment_join']['display_date'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Display original date at the beginning of enlarged comments'),
    '#default_value' => _comment_join_variable_get('display_date')
  );

  return system_settings_form($form);
}




